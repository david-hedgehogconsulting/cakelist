# Cake List Sample App

This app retrieves and displays a sorted list of cakes with duplicate entries removed

The list supports pull to refresh functionality

Tapping a cake item will display the cake description in a snack bar popup

The app demonstrates the MVVM architectural pattern

### Libraries
**Koin** is used to handle dependency injection

**RxJava** is used to handle streams of data 

**Mockk** is used to handle unit testing 

**Retrofit** is used for accessing the cakes API

**Moshi** is used for JSON parsing

**Picasso** is used for image loading
