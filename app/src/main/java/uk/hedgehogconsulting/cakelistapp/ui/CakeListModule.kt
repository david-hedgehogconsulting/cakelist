package uk.hedgehogconsulting.cakelistapp.ui

import com.squareup.picasso.Picasso
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

fun cakeListModule() = module {

    viewModel { CakeListViewModel(get()) }

    single { Picasso.Builder(androidContext()).build() }

    factory { CakeListAdapter(get()) }
}
