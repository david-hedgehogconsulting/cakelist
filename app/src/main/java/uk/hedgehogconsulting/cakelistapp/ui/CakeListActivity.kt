package uk.hedgehogconsulting.cakelistapp.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.google.android.material.snackbar.Snackbar
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_cake_list.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import uk.hedgehogconsulting.cakelistapp.R
import uk.hedgehogconsulting.cakelistapp.domain.model.Cake
import uk.hedgehogconsulting.cakelistapp.util.OneShot
import java.io.IOException

class CakeListActivity : AppCompatActivity() {

    private val viewModel by viewModel<CakeListViewModel>()

    private val adapter: CakeListAdapter by inject()

    private val disposables = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cake_list)
        setActionBar(toolbar)

        cake_list.adapter = adapter

        swipe_container.setOnRefreshListener {
            viewModel.fetchCakes()
        }

        setupObservers()

        if (savedInstanceState == null) {
            viewModel.fetchCakes()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        disposables.clear()
    }

    private fun setupObservers() {
        viewModel.cakesLiveData.observe(this, Observer<List<Cake>> { items ->
            items?.let { updateCakeList(it) }
        })
        viewModel.errorLiveData.observe(this, Observer<OneShot<Throwable>> { error ->
            error.value?.let { showError(it) }
        })
        adapter.onClick()
            .subscribe { showDescription(it) }
            .addTo(disposables)
    }

    private fun updateCakeList(cakes: List<Cake>) {
        swipe_container.isRefreshing = false
        adapter.items = cakes
    }

    private fun showError(throwable: Throwable) {
        swipe_container.isRefreshing = false
        val messageId = when(throwable) {
            is IOException -> R.string.network_error
            else -> R.string.unknown_error
        }
        Snackbar.make(container_view, messageId, Snackbar.LENGTH_LONG)
            .setAction(R.string.retry) { viewModel.fetchCakes() }
            .show()
    }

    private fun showDescription(cake: Cake) {
        // TODO - do something nicer here like navigate to a cake detail screen
        Snackbar.make(container_view, "${cake.title}: ${cake.description}", Snackbar.LENGTH_LONG)
            .show()
    }
}
