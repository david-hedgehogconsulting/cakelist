package uk.hedgehogconsulting.cakelistapp.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.item_cake.view.*
import uk.hedgehogconsulting.cakelistapp.R
import uk.hedgehogconsulting.cakelistapp.domain.model.Cake

class CakeListAdapter(private val picasso: Picasso) :
    RecyclerView.Adapter<CakeListAdapter.ViewHolder>() {

    private val clickSubject = PublishSubject.create<Cake>()

    var items: List<Cake>
        get() = differ.currentList
        set(value) = differ.submitList(value)

    private val diffCallback = object : DiffUtil.ItemCallback<Cake>() {
        override fun areItemsTheSame(oldItem: Cake, newItem: Cake): Boolean {
            return oldItem.title == newItem.title
        }

        override fun areContentsTheSame(oldItem: Cake, newItem: Cake): Boolean {
            return oldItem == newItem
        }
    }

    private val differ = AsyncListDiffer(this, diffCallback)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        picasso.load(differ.currentList[position].imageUrl)
            .fit()
            .placeholder(R.drawable.ic_cake_slice)
            .into(holder.imageView)
        holder.titleView.text = differ.currentList[position].title
        holder.container.setOnClickListener { clickSubject.onNext(differ.currentList[position]) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_cake, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    fun onClick(): Observable<Cake> = clickSubject.hide()

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val container: View = itemView.cake_container
        val imageView: ImageView = itemView.cake_image
        val titleView: TextView = itemView.cake_title
    }
}
