package uk.hedgehogconsulting.cakelistapp.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import uk.hedgehogconsulting.cakelistapp.domain.model.Cake
import uk.hedgehogconsulting.cakelistapp.domain.usecase.GetCakesUseCase
import uk.hedgehogconsulting.cakelistapp.util.OneShot

class CakeListViewModel(
    private val getCakesUseCase: GetCakesUseCase
) : ViewModel() {

    private val disposables = CompositeDisposable()

    private val _cakesLiveData = MutableLiveData<List<Cake>>()
    private val _errorLiveData = MutableLiveData<OneShot<Throwable>>()

    val cakesLiveData: LiveData<List<Cake>> get() = _cakesLiveData
    val errorLiveData: LiveData<OneShot<Throwable>> get() = _errorLiveData

    fun fetchCakes() =
        getCakesUseCase.execute()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onSuccess = { _cakesLiveData.value = it },
                onError = { _errorLiveData.value = OneShot(it) }
            )
            .addTo(disposables)

    override fun onCleared() {
        disposables.clear()
    }
}
