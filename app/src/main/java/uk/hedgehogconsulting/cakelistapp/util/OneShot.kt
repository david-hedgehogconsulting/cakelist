package uk.hedgehogconsulting.cakelistapp.util

/**
 * Used when we want to use LiveData to observe one shot events e.g. errors
 */
class OneShot<out T>(private val content: T) {

    private var hasBeenRead = false

    val value: T?
        get() =
            if (hasBeenRead) {
                null
            } else {
                hasBeenRead = true
                content
            }
}
