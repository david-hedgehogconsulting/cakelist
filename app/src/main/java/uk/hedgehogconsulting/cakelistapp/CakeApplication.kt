package uk.hedgehogconsulting.cakelistapp

import android.app.Application
import uk.hedgehogconsulting.cakelistapp.di.KoinContext

class CakeApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        KoinContext.initialise(this)
    }
}
