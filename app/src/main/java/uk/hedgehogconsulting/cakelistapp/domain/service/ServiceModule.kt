package uk.hedgehogconsulting.cakelistapp.domain.service

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import org.koin.dsl.module

private const val END_POINT = "https://gist.githubusercontent.com/t-reed/739df99e9d96700f17604a3971e701fa/raw/1d4dd9c5a0ec758ff5ae92b7b13fe4d57d34e1dc/waracle_cake-android-client/"

fun serviceModule() = module {

    single { Moshi.Builder().add(KotlinJsonAdapterFactory()).build() }
    single {
        RestServiceFactory.createRetrofitService(
            CakeService::class.java,
            END_POINT,
            get()
        )
    }
}
