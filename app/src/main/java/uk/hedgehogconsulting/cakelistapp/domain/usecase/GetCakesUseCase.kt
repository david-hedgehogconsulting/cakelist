package uk.hedgehogconsulting.cakelistapp.domain.usecase

import io.reactivex.Single
import uk.hedgehogconsulting.cakelistapp.domain.model.Cake
import uk.hedgehogconsulting.cakelistapp.domain.repository.CakeRepository

/**
 * Trivial class just to illustrate use case layer in MVVM architecture
 * Normally use cases would implement domain specific business logic
 */
class GetCakesUseCase(private val cakeRepository: CakeRepository) {

    fun execute(): Single<List<Cake>> =
        cakeRepository.getCakes()
}
