package uk.hedgehogconsulting.cakelistapp.domain.usecase

import org.koin.dsl.module

fun useCaseModule() = module {

    single { GetCakesUseCase(get()) }
}
