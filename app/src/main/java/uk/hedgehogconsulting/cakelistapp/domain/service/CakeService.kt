package uk.hedgehogconsulting.cakelistapp.domain.service

import io.reactivex.Single
import retrofit2.http.GET
import uk.hedgehogconsulting.cakelistapp.domain.model.Cake

interface CakeService {
    @GET("./")
    fun getCakes(): Single<List<Cake>>
}
