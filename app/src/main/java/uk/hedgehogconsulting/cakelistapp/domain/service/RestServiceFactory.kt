package uk.hedgehogconsulting.cakelistapp.domain.service

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.squareup.moshi.Moshi

import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object RestServiceFactory {
    fun <T> createRetrofitService(clazz: Class<T>, endPoint: String, moshi: Moshi): T {
        val retrofit = Retrofit.Builder()
            .baseUrl(endPoint)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build()
        return retrofit.create(clazz)
    }
}
