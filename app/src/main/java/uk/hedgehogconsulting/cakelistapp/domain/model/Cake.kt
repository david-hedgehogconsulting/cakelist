package uk.hedgehogconsulting.cakelistapp.domain.model

import com.squareup.moshi.Json

data class Cake(
    @Json(name = "title") val title: String,
    @Json(name = "desc") val description: String,
    @Json(name = "image") val imageUrl: String
)
