package uk.hedgehogconsulting.cakelistapp.domain.repository

import io.reactivex.Single
import uk.hedgehogconsulting.cakelistapp.domain.model.Cake
import uk.hedgehogconsulting.cakelistapp.domain.service.CakeService

class CakeRepository(private val cakeService: CakeService) {

    fun getCakes(): Single<List<Cake>> =
        cakeService.getCakes()
            .toObservable()
            .flatMapIterable { it }
            .distinct()
            .toSortedList { cake1, cake2 -> cake1.title.compareTo(cake2.title) }
}
