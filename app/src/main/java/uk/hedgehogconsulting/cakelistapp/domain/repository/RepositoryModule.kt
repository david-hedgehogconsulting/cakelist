package uk.hedgehogconsulting.cakelistapp.domain.repository

import org.koin.dsl.module

fun repositoryModule() = module {

    single { CakeRepository(get()) }
}
