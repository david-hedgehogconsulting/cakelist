package uk.hedgehogconsulting.cakelistapp.di

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.KoinComponent
import org.koin.core.context.startKoin
import uk.hedgehogconsulting.cakelistapp.domain.repository.repositoryModule
import uk.hedgehogconsulting.cakelistapp.domain.service.serviceModule
import uk.hedgehogconsulting.cakelistapp.domain.usecase.useCaseModule
import uk.hedgehogconsulting.cakelistapp.ui.cakeListModule

object KoinContext : KoinComponent {

    fun initialise(application: Application) {
        val modules = listOf(
            serviceModule(),
            repositoryModule(),
            useCaseModule(),
            cakeListModule()
        )
        startKoin {
            modules(modules)
                .androidContext(application)
        }
    }
}
