package uk.hedgehogconsulting.cakelistapp.domain.repository

import io.mockk.confirmVerified
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Single
import org.junit.After
import org.junit.Test
import uk.hedgehogconsulting.cakelistapp.domain.model.Cake
import uk.hedgehogconsulting.cakelistapp.domain.service.CakeService

class CakeRepositoryTest {

    private companion object {
        val CAKE1 = Cake("cake1","c cake 1 desc", "z cake 1 image")
        val CAKE2 = Cake("cake2","b cake 2 desc", "y cake 2 image")
        val CAKE3 = Cake("cake3","a cake 3 desc", "x cake 3 image")
    }

    private val cakeService = mockk<CakeService>()

    private val cakeRepository: CakeRepository by lazy {
        CakeRepository(cakeService)
    }

    @Test
    fun `getCakes returns cakes`() {

        every { cakeService.getCakes() } returns Single.just(listOf(CAKE1, CAKE2, CAKE3))

        val testObserver = cakeRepository.getCakes().test()

        testObserver.assertComplete()
        verify { cakeService.getCakes() }
        testObserver.assertValue(listOf(CAKE1, CAKE2, CAKE3))
    }

    @Test
    fun `getCakes sorts cakes`() {

        every { cakeService.getCakes() } returns Single.just(listOf(CAKE3, CAKE2, CAKE1))

        val testObserver = cakeRepository.getCakes().test()

        testObserver.assertComplete()
        verify { cakeService.getCakes() }
        testObserver.assertValue(listOf(CAKE1, CAKE2, CAKE3))
    }

    @Test
    fun `getCakes de-dupes cakes by title`() {

        every { cakeService.getCakes() } returns Single.just(listOf(CAKE1, CAKE2, CAKE3, CAKE1, CAKE2, CAKE3))

        val testObserver = cakeRepository.getCakes().test()

        testObserver.assertComplete()
        verify { cakeService.getCakes() }
        testObserver.assertValue(listOf(CAKE1, CAKE2, CAKE3))
    }

    @After
    fun tearDown() {
        confirmVerified(cakeService)
    }
}
