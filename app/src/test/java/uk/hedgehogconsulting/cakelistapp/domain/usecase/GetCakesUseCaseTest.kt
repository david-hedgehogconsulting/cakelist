package uk.hedgehogconsulting.cakelistapp.domain.usecase

import io.mockk.confirmVerified
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Single
import org.junit.After
import org.junit.Test
import uk.hedgehogconsulting.cakelistapp.domain.model.Cake
import uk.hedgehogconsulting.cakelistapp.domain.repository.CakeRepository

class GetCakesUseCaseTest {

    private companion object {
        val CAKE1 = Cake("cake1","cake 1 desc", "cake 1 image")
        val CAKE2 = Cake("cake2","cake 2 desc", "cake 2 image")
        val CAKE3 = Cake("cake3","cake 3 desc", "cake 3 image")
    }

    private val cakeRepository = mockk<CakeRepository>()

    private val getCakesUseCase: GetCakesUseCase by lazy {
        GetCakesUseCase(cakeRepository)
    }

    @Test
    fun `returns list of cakes`() {
        every { cakeRepository.getCakes() } returns Single.just(listOf(CAKE1, CAKE2, CAKE3))

        val testObserver = getCakesUseCase.execute().test()

        testObserver.assertComplete()
        verify { cakeRepository.getCakes() }
        testObserver.assertValue(listOf(CAKE1, CAKE2, CAKE3))
    }

    @After
    fun tearDown() {
        confirmVerified(cakeRepository)
    }
}
