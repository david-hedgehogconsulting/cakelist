package uk.hedgehogconsulting.cakelistapp.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import io.mockk.confirmVerified
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.TestScheduler
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import uk.hedgehogconsulting.cakelistapp.domain.model.Cake
import uk.hedgehogconsulting.cakelistapp.domain.usecase.GetCakesUseCase

class CakeListViewModelTest {

    private companion object {
        val CAKE1 = Cake("cake1","cake 1 desc", "cake 1 image")
        val CAKE2 = Cake("cake2","cake 2 desc", "cake 2 image")
        val CAKE3 = Cake("cake3","cake 3 desc", "cake 3 image")
    }

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private val testScheduler = TestScheduler()

    private val getCakesUseCase = mockk<GetCakesUseCase>()
    private val throwable = mockk<Throwable>(relaxed = true)

    private val viewModel: CakeListViewModel by lazy {
        CakeListViewModel(getCakesUseCase)
    }

    @Before
    fun setUp() {
        RxJavaPlugins.setInitIoSchedulerHandler { testScheduler }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { testScheduler }
        RxJavaPlugins.setIoSchedulerHandler { testScheduler }
        RxAndroidPlugins.setMainThreadSchedulerHandler { testScheduler }
    }

    @Test
    fun `successful fetchCakes() call triggers cakesLiveData`() {
        every { getCakesUseCase.execute() } returns Single.just(listOf(CAKE1, CAKE2, CAKE3))

        viewModel.fetchCakes()

        testScheduler.triggerActions()

        verify { getCakesUseCase.execute() }
        assertEquals(listOf(CAKE1, CAKE2, CAKE3), viewModel.cakesLiveData.value)
        assertEquals(null, viewModel.errorLiveData.value)
    }

    @Test
    fun `failing fetchCakes() call triggers error LiveData`() {
        every { getCakesUseCase.execute() } returns Single.error(throwable)

        viewModel.fetchCakes()

        testScheduler.triggerActions()

        verify { getCakesUseCase.execute() }
        assertEquals(null, viewModel.cakesLiveData.value)
        assertEquals(throwable, viewModel.errorLiveData.value?.value)
    }

    @After
    fun tearDown() {
        confirmVerified(getCakesUseCase)
    }
}
