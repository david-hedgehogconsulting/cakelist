package uk.hedgehogconsulting.cakelistapp.util

import org.junit.Assert.assertEquals
import org.junit.Test

class OneShotTest {

    @Test
    fun `returns value only once`() {

        val oneShot = OneShot("test")

        assertEquals("test", oneShot.value)
        assertEquals(null, oneShot.value)
    }
}
